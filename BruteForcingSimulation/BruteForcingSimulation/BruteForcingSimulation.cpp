// BruteForcingSimulation.cpp : Defines the entry point for the console application.
//

/*
[10]  [0-9]     ID of cracker that succeeded
[14]  [10-23]   Cracked result chars
[135] [24-175]  [24-133=FieldChars][134-137=StartKey][138-151=KeyToFind][152-155=NumberOfAttacks][156-159=SizeOfFieldChars]
*/

#include "stdafx.h"
#include <cstring>
#include <iostream>
#include <time.h>
#include <stdint.h>
#include <thread>
#include <mutex>
#include <vector> 
#include "limits.h"
#include <cstdlib>
#include <stdlib.h>
#include <string.h>
#include "math.h"

#define SIZE_KEY_TO_FIND (100)
#define CLIENT_BLOCK_SIZE (140+SIZE_KEY_TO_FIND)
#define SIZE_OF_HEX_FORMAT (34)
#define INIT_A 0x67452301
#define INIT_B 0xefcdab89
#define INIT_C 0x98badcfe
#define INIT_D 0x10325476
 
#define SQRT_2 0x5a827999
#define SQRT_3 0x6ed9eba1

std::mutex g_threadCount_Mutex;

char itoa16[17] = "0123456789ABCDEF";
void NTLM(char *key, char *hex_format)
{
	unsigned int nt_buffer[16];
	unsigned int output[4];
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Prepare the string for hash calculation
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	int i = 0;
	int length = strlen(key);
	memset(nt_buffer, 0, 16*4);
	//The length of key need to be <= 27
	for(; i<length/2; i++)	
		nt_buffer[i] = key[2 * i] | (key[2 * i + 1] << 16);
 
	//padding
	if(length % 2 == 1)
		nt_buffer[i] = key[length - 1] | 0x800000;
	else
		nt_buffer[i] = 0x80;
	//put the length
	nt_buffer[14] = length << 4;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// NTLM hash calculation
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	unsigned int a = INIT_A;
	unsigned int b = INIT_B;
	unsigned int c = INIT_C;
	unsigned int d = INIT_D;
 
	/* Round 1 */
	a += (d ^ (b & (c ^ d)))  +  nt_buffer[0]  ;a = (a << 3 ) | (a >> 29);
	d += (c ^ (a & (b ^ c)))  +  nt_buffer[1]  ;d = (d << 7 ) | (d >> 25);
	c += (b ^ (d & (a ^ b)))  +  nt_buffer[2]  ;c = (c << 11) | (c >> 21);
	b += (a ^ (c & (d ^ a)))  +  nt_buffer[3]  ;b = (b << 19) | (b >> 13);
 
	a += (d ^ (b & (c ^ d)))  +  nt_buffer[4]  ;a = (a << 3 ) | (a >> 29);
	d += (c ^ (a & (b ^ c)))  +  nt_buffer[5]  ;d = (d << 7 ) | (d >> 25);
	c += (b ^ (d & (a ^ b)))  +  nt_buffer[6]  ;c = (c << 11) | (c >> 21);
	b += (a ^ (c & (d ^ a)))  +  nt_buffer[7]  ;b = (b << 19) | (b >> 13);
 
	a += (d ^ (b & (c ^ d)))  +  nt_buffer[8]  ;a = (a << 3 ) | (a >> 29);
	d += (c ^ (a & (b ^ c)))  +  nt_buffer[9]  ;d = (d << 7 ) | (d >> 25);
	c += (b ^ (d & (a ^ b)))  +  nt_buffer[10] ;c = (c << 11) | (c >> 21);
	b += (a ^ (c & (d ^ a)))  +  nt_buffer[11] ;b = (b << 19) | (b >> 13);
 
	a += (d ^ (b & (c ^ d)))  +  nt_buffer[12] ;a = (a << 3 ) | (a >> 29);
	d += (c ^ (a & (b ^ c)))  +  nt_buffer[13] ;d = (d << 7 ) | (d >> 25);
	c += (b ^ (d & (a ^ b)))  +  nt_buffer[14] ;c = (c << 11) | (c >> 21);
	b += (a ^ (c & (d ^ a)))  +  nt_buffer[15] ;b = (b << 19) | (b >> 13);
 
	/* Round 2 */
	a += ((b & (c | d)) | (c & d)) + nt_buffer[0] +SQRT_2; a = (a<<3 ) | (a>>29);
	d += ((a & (b | c)) | (b & c)) + nt_buffer[4] +SQRT_2; d = (d<<5 ) | (d>>27);
	c += ((d & (a | b)) | (a & b)) + nt_buffer[8] +SQRT_2; c = (c<<9 ) | (c>>23);
	b += ((c & (d | a)) | (d & a)) + nt_buffer[12]+SQRT_2; b = (b<<13) | (b>>19);
 
	a += ((b & (c | d)) | (c & d)) + nt_buffer[1] +SQRT_2; a = (a<<3 ) | (a>>29);
	d += ((a & (b | c)) | (b & c)) + nt_buffer[5] +SQRT_2; d = (d<<5 ) | (d>>27);
	c += ((d & (a | b)) | (a & b)) + nt_buffer[9] +SQRT_2; c = (c<<9 ) | (c>>23);
	b += ((c & (d | a)) | (d & a)) + nt_buffer[13]+SQRT_2; b = (b<<13) | (b>>19);
 
	a += ((b & (c | d)) | (c & d)) + nt_buffer[2] +SQRT_2; a = (a<<3 ) | (a>>29);
	d += ((a & (b | c)) | (b & c)) + nt_buffer[6] +SQRT_2; d = (d<<5 ) | (d>>27);
	c += ((d & (a | b)) | (a & b)) + nt_buffer[10]+SQRT_2; c = (c<<9 ) | (c>>23);
	b += ((c & (d | a)) | (d & a)) + nt_buffer[14]+SQRT_2; b = (b<<13) | (b>>19);
 
	a += ((b & (c | d)) | (c & d)) + nt_buffer[3] +SQRT_2; a = (a<<3 ) | (a>>29);
	d += ((a & (b | c)) | (b & c)) + nt_buffer[7] +SQRT_2; d = (d<<5 ) | (d>>27);
	c += ((d & (a | b)) | (a & b)) + nt_buffer[11]+SQRT_2; c = (c<<9 ) | (c>>23);
	b += ((c & (d | a)) | (d & a)) + nt_buffer[15]+SQRT_2; b = (b<<13) | (b>>19);
 
	/* Round 3 */
	a += (d ^ c ^ b) + nt_buffer[0]  +  SQRT_3; a = (a << 3 ) | (a >> 29);
	d += (c ^ b ^ a) + nt_buffer[8]  +  SQRT_3; d = (d << 9 ) | (d >> 23);
	c += (b ^ a ^ d) + nt_buffer[4]  +  SQRT_3; c = (c << 11) | (c >> 21);
	b += (a ^ d ^ c) + nt_buffer[12] +  SQRT_3; b = (b << 15) | (b >> 17);
 
	a += (d ^ c ^ b) + nt_buffer[2]  +  SQRT_3; a = (a << 3 ) | (a >> 29);
	d += (c ^ b ^ a) + nt_buffer[10] +  SQRT_3; d = (d << 9 ) | (d >> 23);
	c += (b ^ a ^ d) + nt_buffer[6]  +  SQRT_3; c = (c << 11) | (c >> 21);
	b += (a ^ d ^ c) + nt_buffer[14] +  SQRT_3; b = (b << 15) | (b >> 17);
 
	a += (d ^ c ^ b) + nt_buffer[1]  +  SQRT_3; a = (a << 3 ) | (a >> 29);
	d += (c ^ b ^ a) + nt_buffer[9]  +  SQRT_3; d = (d << 9 ) | (d >> 23);
	c += (b ^ a ^ d) + nt_buffer[5]  +  SQRT_3; c = (c << 11) | (c >> 21);
	b += (a ^ d ^ c) + nt_buffer[13] +  SQRT_3; b = (b << 15) | (b >> 17);
 
	a += (d ^ c ^ b) + nt_buffer[3]  +  SQRT_3; a = (a << 3 ) | (a >> 29);
	d += (c ^ b ^ a) + nt_buffer[11] +  SQRT_3; d = (d << 9 ) | (d >> 23);
	c += (b ^ a ^ d) + nt_buffer[7]  +  SQRT_3; c = (c << 11) | (c >> 21);
	b += (a ^ d ^ c) + nt_buffer[15] +  SQRT_3; b = (b << 15) | (b >> 17);
 
	output[0] = a + INIT_A;
	output[1] = b + INIT_B;
	output[2] = c + INIT_C;
	output[3] = d + INIT_D;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Convert the hash to hex (for being readable)
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(i=0; i<4; i++)
	{
		int j = 0;
		unsigned int n = output[i];
		//iterate the bytes of the integer		
		for(; j<4; j++)
		{
			unsigned int convert = n % 256;
			hex_format[i * 8 + j * 2 + 1] = itoa16[convert % 16];
			convert = convert / 16;
			hex_format[i * 8 + j * 2 + 0] = itoa16[convert % 16];
			n = n / 256;
		}	
	}
	//null terminate the string
	hex_format[33] = 0;
}

void GetCombinationInBaseFormat(unsigned long long *numericCombination, int *sizeOfFieldCharacters, char *fieldCharacters, int *sizeOfCombinationInBase, char *combinationInBase)
{	
	char converted_number[25]={};
	int next_digit, index=0;	
	if(numericCombination==0)
	{
		combinationInBase[0] = fieldCharacters[0];
		*sizeOfCombinationInBase=1;
	}
	else
	{
		while (*numericCombination != 0)
		{
			converted_number[index] = fieldCharacters[*numericCombination % *sizeOfFieldCharacters];
			*numericCombination = *numericCombination / *sizeOfFieldCharacters;
			++index;
		}
		*sizeOfCombinationInBase = index;
		for(int i=index-1;i>=0;i--)
		{
			combinationInBase[(index-1)-i] = converted_number[i];
		}
	}
}

unsigned long long GetDecimalFromCombination(int sizeOfCombinationInBase, char *combinationInBase, const int sizeOfFieldCharacters, char *fieldCharacters)
{
	unsigned long long result = 0;
	int base = sizeOfFieldCharacters;
	for(int ch=sizeOfCombinationInBase-1;ch>=0;ch--)
	{
		for(int lookup=0;lookup<sizeOfFieldCharacters;lookup++)
		{
			if(fieldCharacters[lookup]==combinationInBase[ch])
			{
				result+=lookup * (pow(sizeOfFieldCharacters,sizeOfCombinationInBase-1-ch));
				break;
			}
		}
	}
	return result;
}

bool IncrementChar(char *charToIncrement, const int sizeOfFieldCharacters, char *fieldCharacters)
{
	for(int fieldCharsIndex=0; fieldCharsIndex<sizeOfFieldCharacters-1; fieldCharsIndex++)
	{
		if(*charToIncrement==fieldCharacters[fieldCharsIndex])
		{
			*charToIncrement=fieldCharacters[fieldCharsIndex+1];
			return true;
			//return fieldCharsIndex<sizeOfFieldCharacters-2;
		}
	}
	return false;
}

bool CompareArrays(int sizeOfArrayA, char *arrayA, int sizeOfArrayB, char *arrayB, char *hex_format)
{
	NTLM(arrayB, hex_format);
	
	bool arraysMatched = true;
	
	/*if(sizeOfArrayB==6)
		if(arrayB[0]=='a')
			if(arrayB[1]=='a')
				if(arrayB[2]=='a')
					if(arrayB[3]=='a')
						if(arrayB[4]=='x')
							if(arrayB[5]=='+')
			
	{
		arraysMatched = true;
	}*/

	//Breaks on char=0
	for(int index=0;index<SIZE_OF_HEX_FORMAT-2;index++)
	{
		if(
			(arrayA[index]!=hex_format[index])
			|| (arrayA[index]==0 && hex_format[index]!=0)
			|| (hex_format[index]==0 && arrayA[index]!=0)
			)		
		{
			arraysMatched = false;
			break;
		}
		else if (arrayA[index]==0 && hex_format[index]==0)
		{
			break;
		}
	}
	return arraysMatched;
}

bool GenerateAllCombinations(unsigned long long *currentCombinationCounter, unsigned long long *combinationsToTryPerThread, int startIndex, int *sizeOfCombinationInBaseFormat, char *combinationInBaseFormat, const int sizeOfFieldCharacters, char *fieldCharacters, char *keyToFind, char *hex_format)
{
	do
	{
		if(CompareArrays(SIZE_KEY_TO_FIND,keyToFind,*sizeOfCombinationInBaseFormat,combinationInBaseFormat, hex_format))
		{
			return true;
		}
		if(startIndex<*sizeOfCombinationInBaseFormat-1)
		{
			if(GenerateAllCombinations(currentCombinationCounter, combinationsToTryPerThread, startIndex+1, sizeOfCombinationInBaseFormat, combinationInBaseFormat, sizeOfFieldCharacters, fieldCharacters, keyToFind, hex_format))
			{
				return true;
			}
			combinationInBaseFormat[startIndex+1]=fieldCharacters[0];			
		}
		(*currentCombinationCounter)++;
	}while(IncrementChar(&combinationInBaseFormat[startIndex], sizeOfFieldCharacters, fieldCharacters)
		&& *currentCombinationCounter<*combinationsToTryPerThread);

	return false;
}

const std::string currentDateTime() {
    time_t now = time(0);
    struct tm  *tstruct;
    time(&now);
    char       buf[80];
    tstruct = localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", tstruct);

    return buf;
}

void StartGenerating(char *p_buffer, int allocatedRegionOffset)
{		

	#pragma region Thread Counter
	int threadCounter;
	g_threadCount_Mutex.lock();
	std::memcpy(&threadCounter,&p_buffer[1],sizeof(threadCounter));	
	threadCounter++;
	std::memcpy(&p_buffer[1], &threadCounter, sizeof(threadCounter));				
	g_threadCount_Mutex.unlock();
	#pragma endregion
	char hex_format[SIZE_OF_HEX_FORMAT];
	char fieldCharacters[110];
	unsigned long long startOfCombination = 0;	
	unsigned long combinationsToTryPerThread = 0;
	char keyToFind[SIZE_KEY_TO_FIND]={};
	int sizeOfFieldCharsUsed = 0;	
	std::memcpy(&threadCounter,&p_buffer[1],sizeof(threadCounter));
	std::memcpy(fieldCharacters,&p_buffer[allocatedRegionOffset],110);
	std::memcpy(&startOfCombination,&p_buffer[allocatedRegionOffset+110],8);
	std::memcpy(keyToFind,&p_buffer[allocatedRegionOffset+110+8],SIZE_KEY_TO_FIND);		
	std::memcpy(&combinationsToTryPerThread,&p_buffer[allocatedRegionOffset+110+8+SIZE_KEY_TO_FIND],sizeof(combinationsToTryPerThread));
	std::memcpy(&sizeOfFieldCharsUsed,&p_buffer[allocatedRegionOffset+110+8+SIZE_KEY_TO_FIND+8],sizeof(sizeOfFieldCharsUsed));		
	char combinationInBaseFormat[SIZE_KEY_TO_FIND]={};
	int sizeOfCombinationInBaseFormat = 0;	

	#pragma region Get the numeric combination in the correct BASE format (oct, hex, binary, alpha etc)
	GetCombinationInBaseFormat(&startOfCombination, &sizeOfFieldCharsUsed, fieldCharacters, &sizeOfCombinationInBaseFormat, combinationInBaseFormat);
	#pragma endregion

	#pragma region Try all possible combinations for this thread
	unsigned long long endOfCombination = startOfCombination+combinationsToTryPerThread;
	unsigned long long currentCombination=0;
	for(currentCombination=startOfCombination;currentCombination<=endOfCombination;currentCombination++)
	{
		if(GenerateAllCombinations(&currentCombination, &endOfCombination, 0, &sizeOfCombinationInBaseFormat, combinationInBaseFormat, sizeOfFieldCharsUsed, fieldCharacters, keyToFind, hex_format))
		{
			p_buffer[0]='A';
			break;
		}
		if(currentCombination<endOfCombination)
		{
			if(sizeOfCombinationInBaseFormat>10)
			{
				sizeOfCombinationInBaseFormat=sizeOfCombinationInBaseFormat+1;
			}
			for(int ll=0;ll<sizeOfCombinationInBaseFormat+1;ll++)
			{
				combinationInBaseFormat[ll]=fieldCharacters[0];
			}
		}
		sizeOfCombinationInBaseFormat++;
	}
	#pragma endregion

	#pragma region Thread Counter
	g_threadCount_Mutex.lock();
	std::memcpy(&threadCounter,&p_buffer[1],sizeof(threadCounter));	
	threadCounter--;
	std::memcpy(&p_buffer[1], &threadCounter, sizeof(threadCounter));				
	g_threadCount_Mutex.unlock();
	#pragma endregion
}

bool GetCommandLineArguments(int argc, char** argv, int *sizeOfFieldCharsUsed, char *fieldCharacters, char keyToFind[SIZE_KEY_TO_FIND], unsigned int *concurrentThreadsEmployed)
{
	bool cFound=false;
	bool kFound=false;

	std::cout<<std::endl;
	for(int arg=1;arg<argc;arg+=2)
	{		    	  
		if (strcmp(argv[arg], "-c")==0) 
		{
			cFound = true;
			if(strcmp(argv[arg+1], "S1")==0)
			{				
				*sizeOfFieldCharsUsed = *sizeOfFieldCharsUsed;
			}			
			else if(strcmp(argv[arg+1], "S2")==0)
			{				
				*sizeOfFieldCharsUsed = 76;
				fieldCharacters = ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=");
			}
			else if(strcmp(argv[arg+1], "S3")==0)
			{				
				*sizeOfFieldCharsUsed = 82;
				fieldCharacters = ("acdefghikjlmnopqrstwvxz012456789!.* _-$#@<'=/`:])~%([^%}{ \\&EAOSRINLTMCDHPKWFJVZXQGN");
			}
			else
			{
				strcpy(fieldCharacters,argv[arg+1]);
				*sizeOfFieldCharsUsed = strlen(argv[arg+1]);				
			}
			std::cout<<"Using "<<*sizeOfFieldCharsUsed<<" chars: "<<fieldCharacters<<std::endl;
		}
		else if (strcmp(argv[arg], "-k")==0) 
		{
 			kFound = true;
			strcpy(keyToFind,argv[arg+1]);
		}
		else if (strcmp(argv[arg], "-th")==0) 
		{
			*concurrentThreadsEmployed=atoi(argv[arg+1]);
			std::cout<<"Requested to use "<<*concurrentThreadsEmployed<<" CPU threads"<<std::endl;
		}
	}

	if(!cFound || !kFound)
	{	  
		std::cerr<<std::endl<<"Usage : -c S1 -k \"password\""<<std::endl
		<<"  Character set : -c, characters to use in bruteforce"<<std::endl
		<<"  Key to find   : -k, a set of upto 33 characters to find"<<std::endl
		<<"  CPU threads   : -th, number of CPU threads to use"<<std::endl
		<<std::endl
		<<"Examples: "<<std::endl
		<<"  -c S1 -k \"Password123\" : use all upper, lower, numeric "<<std::endl
		<<"                             and special characters to find the"<<std::endl
		<<"                             word Password123"<<std::endl
		<<"  -c \"10\" -k \"10101\" : use custom characters 1 and 0"<<std::endl
		<<"                             to find the sequence 10101"<<std::endl
		<<"  -c \"1234567890\\\\\"':;\" -k \"123\\\":\" : use custom characters"<<std::endl
		<<"                             1234567890\\\"':; to find 123\":"<<std::endl
		<<std::endl
		<<"Note that in arguments character \\ and \" but be prefixed with a \\, so"<<std::endl
		<<"\\ becomes \\\\ and \" becomes \\\""<<std::endl<<std::endl<<std::endl;

		return false;
	}	

	return true;
}

int main(int argc, char **argv)
{
	#define BUFFER_SIZE 96	
	char fieldCharacters[96] ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-=_+[]{}\\|;:'\",.<>/?~` ";
	int sizeOfFieldCharsUsed;
	char keyToFind[SIZE_KEY_TO_FIND] = {};
	unsigned int concurrentThreadsSupported=0;
	sizeOfFieldCharsUsed = sizeof(fieldCharacters)-1;
	std::cout<<"Width: "<<sizeof(unsigned long long)<<std::endl;
	if(!GetCommandLineArguments(argc, argv, &sizeOfFieldCharsUsed, fieldCharacters, keyToFind, &concurrentThreadsSupported))
	{
		return 1;
	}
	else
	{
		//char base[] = ("     ");
		//unsigned long long f = GetDecimalFromCombination(5,base, sizeOfFieldCharsUsed, fieldCharacters );

		#pragma region Get max number of simultanous threads to use		
		if(concurrentThreadsSupported==0)
		{
			concurrentThreadsSupported = std::thread::hardware_concurrency();
			if(concurrentThreadsSupported==-1) {concurrentThreadsSupported = 3;}
			else if(concurrentThreadsSupported==0) {concurrentThreadsSupported = 1;}
		}
		#pragma endregion

		#pragma region Create memory buffer
		char *globalMemoryBuffer;
		uint16_t bufferSize = 25 + (concurrentThreadsSupported*CLIENT_BLOCK_SIZE);
		globalMemoryBuffer = (char *)malloc(sizeof(char) + bufferSize);
		#pragma endregion

		#pragma region Initialise buffer to zeros
		for(int charIndex=0;charIndex<bufferSize;charIndex++)
		{
			globalMemoryBuffer[charIndex] = 0;
		}
		#pragma endregion
		
		//char fieldCharacters[] = "0123456789";
		//char fieldCharacters[] = "01";
		unsigned long long lastAttackBlockEndingBorder = 0;
		unsigned long long start;
		unsigned long long *p_start = &start;
		unsigned long combinationsToTryPerThread = 50000000;

		char combinationInBase[SIZE_KEY_TO_FIND]={};
		int sizeOfCombinationInBase = 0;	
		//char keyToFind[SIZE_KEY_TO_FIND] = {'6','2','3','2','1'};
		//char keyToFind[SIZE_KEY_TO_FIND] = {'1','2','2','0','2','1'};
		//char keyToFind[SIZE_KEY_TO_FIND] = {'1','0','1','1','1'};
		int threadsRunning=0;
		std::memcpy(&globalMemoryBuffer[1], &threadsRunning, sizeof(threadsRunning)); //ThreadCounter
	
		time_t startTime = time(0);
		std::cout<<currentDateTime().c_str()<<": Start..."<<std::endl;

		for(long mainLoop=0;mainLoop<LONG_MAX;mainLoop++)
		{		
			#pragma region Pepare memory buffer
			for(int regionToCrackWith=0;regionToCrackWith<concurrentThreadsSupported*CLIENT_BLOCK_SIZE;regionToCrackWith+=CLIENT_BLOCK_SIZE)
			{			
				start = lastAttackBlockEndingBorder;			
				std::memcpy(&globalMemoryBuffer[25+(regionToCrackWith)], fieldCharacters, sizeof(fieldCharacters));									//FieldChars
				std::memcpy(&globalMemoryBuffer[25+(regionToCrackWith)+110], &lastAttackBlockEndingBorder, sizeof(lastAttackBlockEndingBorder));	//StartKey
				std::memcpy(&globalMemoryBuffer[25+(regionToCrackWith)+110+8], keyToFind, sizeof(keyToFind));										//KeyToFind
				std::memcpy(&globalMemoryBuffer[25+(regionToCrackWith)+110+8+SIZE_KEY_TO_FIND], &combinationsToTryPerThread, sizeof(combinationsToTryPerThread));	//Number Of Combinations to try
				std::memcpy(&globalMemoryBuffer[25+(regionToCrackWith)+110+8+SIZE_KEY_TO_FIND+8], &sizeOfFieldCharsUsed, sizeof(sizeOfFieldCharsUsed));			//SizeOfFieldChars
				lastAttackBlockEndingBorder+=combinationsToTryPerThread;
			}
			#pragma endregion

			#pragma region Kick-off processing
			//concurentThreadsSupported=3;
			//std::vector<std::thread> threads;
			for(int thread=0;thread<concurrentThreadsSupported;thread++)
			{
				do
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(25));
					g_threadCount_Mutex.lock();
					std::memcpy(&threadsRunning,&globalMemoryBuffer[1],sizeof(threadsRunning));				
					g_threadCount_Mutex.unlock();
					if(globalMemoryBuffer[0]!=0){break;}
				}while(threadsRunning>concurrentThreadsSupported-1);
				if(globalMemoryBuffer[0]!=0){break;}

				std::thread thr(StartGenerating,globalMemoryBuffer,25+(thread * CLIENT_BLOCK_SIZE));
				thr.detach();
				//StartGenerating(globalMemoryBuffer, 25+(thread * CLIENT_BLOCK_SIZE));			
			}
			#pragma  endregion				
		
			#pragma region Show Output
			time_t now = time(0);
			double diff = difftime(now, startTime);
			if(diff>0||globalMemoryBuffer[0]!=0)
			{
				unsigned long long key = lastAttackBlockEndingBorder;
				unsigned long long *p_key = &key;
				GetCombinationInBaseFormat(&key, &sizeOfFieldCharsUsed, fieldCharacters, &sizeOfCombinationInBase, combinationInBase);		
				//diff = 101010;
				//1:4:3:30
				int days = floor(diff/86400);//1
				int hours = floor((diff-(days*86400))/3600);//4
				int minutes = floor((diff - (days*86400) - (hours*3600))/60);//3
				int seconds = floor((diff - (days*86400) - (hours*3600)) - (minutes*60));//30
				if(globalMemoryBuffer[0]!=0)
				{
					std::cout<<currentDateTime().c_str()<<": Done: "
						<<": "<<days<<"days "<<hours<<":"<<minutes<<":"<<seconds<<": "<<keyToFind<<std::endl;
		
					break;
				}
				else
				{				
					std::cout<<currentDateTime().c_str()
						<<": "<<days<<"days "<<hours<<":"<<minutes<<":"<<seconds
						<<": "<<(long)(lastAttackBlockEndingBorder/diff)<<" c/s,  Submitted: "<<combinationInBase<<std::endl;
				}
			}	
			#pragma endregion
		}

		return 0;
	}
}


